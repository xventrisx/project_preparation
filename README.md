# How to work it

### Start project on dev station

**For Linux and Mac OS**

1. `make build` - собрать проект
2. `make up` - поднять проект
3. `make log` - используется для вывода лога в консоль


**For Windows**
1. `docker-compose up --build` - собрать и поднять проект
